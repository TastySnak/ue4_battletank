// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4_BattleTankGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4_BATTLETANK_API AUE4_BattleTankGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
